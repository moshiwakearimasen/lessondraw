

<?php require_once("includes/connection.php"); ?>
<!DOCTYPE html>
<html>
 <head>
  <meta charset="utf-8">
  <link rel="stylesheet" href="css/style.css">
    <link rel="stylesheet" href="css/fonts-v3.css">
  <title>Уроки рисования</title>

<link href="css/bootstrap.css" media="screen" rel="stylesheet">
 <link href="css/bootstrap-responsive.css" rel="stylesheet">

<script type="text/javascript" src="js/jquery-latest.js"></script>
<script type="text/javascript" src="js/jquery.tablesorter.min.js"></script>
<script type="text/javascript" src="js/jquery.tablesorter.pager.js"></script>
<script type="text/javascript" src="js/script.js"></script>
</head>
<body>
 <script src="js/bootstrap.js"></script>
 <div class="page-header"><h1>LessonDraw.esy.es</h1><small>обучающий web-ресурс</small></div>
 